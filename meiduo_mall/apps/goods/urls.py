from django.urls import path
from apps.goods.views import IndexView, ListView, HotGoodsView
from apps.goods.views import SKUSearchView, DetailView, CategoryVisitCountView

urlpatterns = [
    # 查询首页图片
    path('index/', IndexView.as_view()),
    # 查询产品列表
    path('list/<category_id>/skus/', ListView.as_view()),
    # 查询热销产品
    path('hot/<category_id>/', HotGoodsView.as_view()),
    # 搜索路由--千万注意: 没有as_view()
    path('search/', SKUSearchView()),
    # 查询详情例子
    path('detail/<sku_id>/', DetailView.as_view()),
    # 统计埋点商品详情
    path('detail/visit/<category_id>/', CategoryVisitCountView.as_view()),

]