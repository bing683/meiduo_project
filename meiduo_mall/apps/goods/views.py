from django.shortcuts import render

# Create your views here.
############上传图片的代码################################
# from fdfs_client.client import Fdfs_client
#
# # 1. 创建客户端
# # 修改加载配置文件的路径
# client = Fdfs_client('utils/fastdfs/client.conf')
#
# # 2. 上传图片
# # 图片的绝对路径
# client.upload_by_filename('logs/111.jpg')
from django.views import View
from utils.goods import get_categories
from apps.contents.models import ContentCategory
from django.http import JsonResponse
from apps.goods.models import GoodsCategory
from apps.goods.models import SKU
from utils.goods import get_breadcrumb
class IndexView(View):
    def get(self, request):

        # 1 查询商品分类数据
        categories = get_categories()
        # 2 广告数据
        contents = {}
        contents_categories = ContentCategory.objects.all()
        for cat in contents_categories:
            contents[cat.key] = cat.content_set.filter(status=True).order_by('sequence')
        # 我们把数据 传递 给 模板
        context = {
            'categories': categories,
            'contents': contents
        }
        # 模板使用比较少，以后大家到公司 自然就会了
        return render(request, 'index.html', context)

class ListView(View):
    def get(self, request, category_id):
        # 1 接收参数
        ordering = request.GET.get('ordering')
        page = request.GET.get('page')
        page_size = request.GET.get('page_size')

        # 2 获取分类
        # 3.根据分类id进行分类数据的查询验证
        try:
            category = GoodsCategory.objects.get(id=category_id)
        except GoodsCategory.DoesNotExist:
            return JsonResponse({'code': 400, 'errmsg': '参数缺失'})

        # 4.获取面包屑数据
        breadcrumb = get_breadcrumb(category)

        # 5.查询分类对应的sku数据,然后排序，分页
        skus = SKU.objects.filter(category=category, is_launched=True).order_by(ordering)

        from django.core.paginator import Paginator
        # object_list, per_page
        # object_list   列表数据
        # per_page      每页多少条数据
        paginator = Paginator(skus, per_page=page_size)
        # 获取指定页码的数据
        page_skus = paginator.page(page)

        sku_list = []
        # 将对象转换为字典数据
        for sku in page_skus.object_list:
            sku_list.append({
                'id': sku.id,
                'name': sku.name,
                'price': sku.price,
                'default_image_url': sku.default_image.url
            })

        # 获取总页码
        total_num = paginator.num_pages

        # 6.返回响应
        return JsonResponse({'code': 0, 'errmsg': '商品列表成功', 'list': sku_list, 'count': total_num, 'breadcrumb': breadcrumb})


class HotGoodsView(View):
    def get(self, request, category_id):

        if category_id is None:
            return JsonResponse({'code': 400, 'errmsg': '参数错误'})
        """提供商品热销排行JSON数据"""
        # 根据销量倒序
        skus = SKU.objects.filter(category_id=category_id, is_launched=True).order_by('-sales')[:2]

        # 序列化
        hot_skus = []
        for sku in skus:
            hot_skus.append({
                'id': sku.id,
                'default_image_url': sku.default_image.url,
                'name': sku.name,
                'price': sku.price
            })

        return JsonResponse({'code': 0, 'errmsg': 'OK', 'hot_skus': hot_skus})

from haystack.views import SearchView
class SKUSearchView(SearchView):
    def create_response(self):
        context = self.get_context()

        suk_list = []
        for sku in context['page'].object_list:
            suk_list.append({
                'id': sku.object.id,
                'name': sku.object.name,
                'price': sku.object.price,
                'default_image_url': sku.object.default_image.url,
                'searchkey': context.get('query'),
                'page_size': context['page'].paginator.num_pages,
                'count': context['page'].paginator.count
            })

        return JsonResponse(suk_list, safe=False)


from utils.goods import get_goods_specs
from datetime import date
class DetailView(View):
    def get(self, request, sku_id):
        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            pass

        # 1 查询分类数据
        categories = get_categories()
        # 2 查询面包屑
        breadcrumb = get_breadcrumb(sku.category)

        # 3 SKU 信息
        # 4 规格信息
        goods_specs = get_goods_specs(sku)

        context = {
            'categories': categories,
            'breadcrumb': breadcrumb,
            'sku': sku,
            'specs': goods_specs,
        }
        return render(request, 'detail.html', context)

from apps.goods.models import GoodsVisitCount
class CategoryVisitCountView(View):
    def post(self, request, category_id):
        # 接收分类
        try:
            category = GoodsCategory.objects.get(id=category_id)
        except GoodsCategory.DoesNotExist:
            return JsonResponse({'code': 400, 'errmsg': '没有此分类'})

        # 查询当天数据
        today = date.today()
        try:
            gvc = GoodsVisitCount.objects.get(category=category, date=today)
        except GoodsVisitCount.DoesNotExist:
            GoodsVisitCount.objects.create(category=category,
                                           date=today,
                                           count=1)
        else:
            gvc.count += 1
            gvc.save()
        return JsonResponse({'code': 0, 'errmsg': 'ok'})
