from django.urls import path
from apps.payment.views import PayUrlView, PaymentStatusView

urlpatterns = [
    # 保存支付宝订单
    path('payment/status/', PaymentStatusView.as_view()),
    # 连接支付宝
    path('payment/<order_id>/', PayUrlView.as_view()),


]