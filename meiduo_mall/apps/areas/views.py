from django.shortcuts import render

# Create your views here.

from django.views import View
from apps.areas.models import Area
from django.http import JsonResponse
from django.core.cache import cache
class AreaView(View):
    def get(self, request):
        # 查询缓存
        province_list = cache.get('province')
        if province_list is None:
            provinces = Area.objects.filter(parent=None)

            province_list = []
            for province in provinces:
                province_list.append({'id': province.id,
                                     'name': province.name})
            #保存缓存
            cache.set('province', province_list, 24*3600)

        return JsonResponse({'code': 0, 'errmsg': 'OK', 'province_list': province_list})

class SubAreaView(View):
    def get(self, request, id):
        # 查询缓存
        data_list = cache.get('city:%s' % id)
        if data_list is None:
            # provinces = Area.objects.filter(parent=id)
            up_level = Area.objects.get(id=id)
            down_level = up_level.subs.all()
            data_list = []
            for item in down_level:
                data_list.append({'id': item.id,
                                   'name': item.name})
            # 缓存数据
            cache.set('city:%s' % id, data_list, 24 * 3600)

        return JsonResponse({'code': 0, 'errmsg': 'OK', 'sub_data': {'subs': data_list}})