from django.urls import path
from apps.oauth.views import QQLoginURLView, QQAuthUserView
urlpatterns = [
    path('qq/authorization/', QQLoginURLView.as_view()),
    path('oauth_callback/', QQAuthUserView.as_view()),
]