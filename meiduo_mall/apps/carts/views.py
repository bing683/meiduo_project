from django.shortcuts import render
from django.views import View
import json
# Create your views here.
from apps.goods.models import SKU
from django.http.response import JsonResponse
import pickle
import base64
from django_redis import get_redis_connection
class CartsView(View):
    # 添加购物车
    def post(self, request):
        # 1.接收数据
        data = json.loads(request.body.decode())
        sku_id = data.get('sku_id')
        count = data.get('count')
        # 2.验证数据
        if not all([sku_id, count]):
            return JsonResponse({'code': 400, 'errmsg': '缺少必传参数'})
        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return JsonResponse({'code': 400, 'errmsg': '查无此商品'})

        try:
            count = int(count)
        except Exception as es:
            count = 1

        # 3.判断用户的登录状态
        user = request.user
        if user.is_authenticated:

            # 4.登录用户保存redis
            # 4.1连接redis
            redis_cli = get_redis_connection('carts')
            pipline = redis_cli.pipeline()
            # 4.2操作hash
            # redis_cli.hset('carts_%s' % user.id, sku_id, count)
            # 会累积操作
            pipline.hincrby('carts_%s' % user.id, sku_id, count)
            # # 4.3操作set
            # redis_cli.sadd('selected_%s' % user.id, sku_id)
            pipline.sadd('selected_%s' % user.id, sku_id)
            pipline.execute()
            # 4.4返回响应
            return JsonResponse({'code':0,'errmsg':'ok'})
        else:
            # 5.未登录用户保存cookie
            cookie_carts = request.COOKIES.get('carts')
            if cookie_carts:
                carts = pickle.loads(base64.b64decode(cookie_carts))
            else:
                # 5.1先有cookie字典
                carts = {}
            # 判断新增的商品 有没有在购物车里
            if sku_id in carts:
                # 购物车中 已经有该商品id
                # 数量累加
                origin_count = carts[sku_id]['count']
                count += origin_count

            carts[sku_id] = {
                'count': count,
                'selected': True
            }
            # 5.2字典转换为bytes
            carts_bytes = pickle.dumps(carts)
            # 5.3bytes类型数据base64编码
            base64encode = base64.b64encode(carts_bytes)
            # 5.4设置cookie
            response = JsonResponse({'code': 0, 'errmsg': 'ok'})
            response.set_cookie('carts', base64encode.decode(), max_age=3600*24*12)
            # 5.5返回响应
            return response

    # 查询购物车
    def get(self, request):
        # 1.判断用户是否登录
        user = request.user
        if user.is_authenticated:
            # 2.登录用户查询redis
            redis_cli = get_redis_connection('carts')
            #     2.1 连接redis
            #     2.2 hash        {sku_id:count}
            sku_id_counts = redis_cli.hgetall('carts_%s' % user.id)
            #     2.3 set         {sku_id}
            selected_ids = redis_cli.smembers('selected_%s' % user.id)
            #     2.4 遍历判断
            carts = {}
            for sku_id, count in sku_id_counts.items():
                carts[int(sku_id)] = {
                    'count': int(count),
                    'selected': sku_id in selected_ids
                }
        else:
            # 3.未登录用户查询cookie
            cookie_carts = request.COOKIES.get('carts')
            #     3.1 读取cookie数据
            #     3.2 判断是否存在购物车数据
            if cookie_carts is not None:
                #         如果存在，则解码            {sku_id:{count:xxx,selected:xxx}}
                carts = pickle.loads(base64.b64decode(cookie_carts))
                #         如果不存在，初始化空字典
            else:
                carts = {}
        #     3.3 根据商品id查询商品信息
        sku_ids = carts.keys()
        #     3.4 将对象数据转换为字典数据
        skus = SKU.objects.filter(id__in=sku_ids)
        sku_list = []
        # 5 将对象数据转换为字典数据
        for sku in skus:
            sku_list.append({
                 'id': sku.id,
                 'price': sku.price,
                 'name': sku.name,
                 'default_image_url': sku.default_image.url,
                 'selected': carts[sku.id]['selected'],  # 选中状态
                 'count': int(carts[sku.id]['count']),  # 数量 强制转换一下
                 'amount': sku.price * carts[sku.id]['count']  # 总价格
             })
        # 6 返回响应
        return JsonResponse({'code': 0, 'errmsg': 'ok', 'cart_skus': sku_list})

    # 修改购物车
    def put(self, request):
        user = request.user
    # 1.获取用户信息
        data = json.loads(request.body.decode())
    # 2.接收数据
        sku_id = data.get('sku_id')
        count = data.get('count')
        selected = data.get('selected')
    # 3.验证数据
        if not all([sku_id, count]):
            return JsonResponse({'code': 400, 'errmsg': '参数不全'})
        try:
            sku = SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return JsonResponse({'code': 400, 'errmsg': '没有此商品'})

        try:
            count = int(count)
        except Exception:
            count = 1
        if user.is_authenticated:
            # 4.登录用户更新redis
            redis_cli = get_redis_connection('carts')
            #     4.1 连接redis

            #     4.2 hash
            redis_cli.hset('carts_%s'% user.id, sku_id, count)
            #     4.3 set
            if selected:
                redis_cli.sadd('selected_%s'% user.id, sku_id)
            else:
                redis_cli.srem('selected_%s'% user.id, sku_id)
            #     4.4 返回响应
            return JsonResponse({'code': 0, 'errmsg': 'ok', 'cart_sku': {'count': count, 'selected': selected}})
        else:
            # 5.未登录用户更新cookie
            cookie_cart = request.COOKIES.get('carts')
            #     5.1 先读取购物车数据
            #         判断有没有。
            #         如果有则解密数据
            #         如果没有则初始化一个空字典
            if cookie_cart is not None:
                carts = pickle.loads(base64.b64decode(cookie_cart))
            else:
                carts = {}
            #     5.2 更新数据
            if sku_id in carts:
                carts[sku_id] = {
                    'count': count,
                    'selected': selected
                }
            #     5.3 重新最字典进行编码和base64加密
            new_carts = base64.b64encode(pickle.dumps(carts))
            #     5.4 设置cookie
            response = JsonResponse({'code': 0, 'errmsg': 'ok', 'cart_sku': {'count': count, 'selected': selected}})
            response.set_cookie('carts', new_carts.decode(), max_age=14*24*3600)
            #     5.5 返回响应
            return response

    # 删除购物车
    def delete(self, request):
        # 1.接收请求
        data = json.loads(request.body.decode())
        sku_id = data.get('sku_id')
        # 2.验证参数
        if sku_id is None:
            return JsonResponse({'code': 400, 'errmsg': '参数不全'})
        try:
            SKU.objects.get(id=sku_id)
        except SKU.DoesNotExist:
            return JsonResponse({'code': 400, 'errmsg': '没有此商品'})
        # 3.根据用户状态
        user = request.user
        if user.is_authenticated:
            # 4.登录用户操作redis
            redis_cli = get_redis_connection('carts')
            #     4.1 连接redis
            #     4.2 hash
            redis_cli.hdel('carts_%s' % user.id, sku_id)
            #     4.3 set
            redis_cli.srem('selected_%s' % user.id, sku_id)
            #     4.4 返回响应
            return JsonResponse({'code': 0, 'errmsg': 'ok'})
        else:
            # 5.未登录用户操作cookie
            #     5.1 读取cookie中的购物车数据
            cookie_cart = request.COOKIES.get('carts')
            #     判断数据是否存在
            if cookie_cart is not None:
                #     存在则解码
                carts = pickle.loads(base64.b64decode(cookie_cart))
            else:
                #     不存在则初始化字典
                carts = {}
            #     5.2 删除数据 {}
            del carts[sku_id]
            #     5.3 我们需要对字典数据进行编码和base64的处理
            new_carts = base64.b64encode(pickle.dumps(carts))
            #     5.4 设置cookie
            response = JsonResponse({'code': 0, 'errmsg': 'ok'})
            response.set_cookie('carts', new_carts.decode(), max_age=14 * 24 * 3600)
            #     5.5 返回响应
            return response

class CartsSimpleView(View):
    def get(self, request):
        user = request.user
        if user.is_authenticated:
            redis_cli = get_redis_connection('carts')
            redis_cart = redis_cli.hgetall('carts_%s' % user.id)
            selected = redis_cli.smembers('slected_%s' % user.id)
            carts = {}
            for sku_id, count in redis_cart.items():
                carts[int(sku_id)] = {
                    'count': int(count),
                    'selected': sku_id in selected
                }
        else:
            cookie_cart = request.COOKIES.get('carts')
            if cookie_cart is not None:
                carts = pickle.loads(base64.b64decode(cookie_cart))
            else:
                carts = {}
        cart_list = []
        suk_ids = carts.keys()
        skus = SKU.objects.filter(id__in=suk_ids)
        for sku in skus:
            cart_list.append({
                'id': sku.id,
                'name': sku.name,
                'count': int(carts[sku.id]['count']),  # 数量 强制转换一下
                'default_image_url': sku.default_image.url
            })

        # 响应json列表数据
        return JsonResponse({'code': 0, 'errmsg': 'OK', 'cart_skus': cart_list})
    
class CartsSelectAllView(View):
    def put(self, request):
        json_dict = json.loads(request.body.decode())
        selected = json_dict.get('selected', True)
        # 校验参数
        if selected:
            if not isinstance(selected, bool):
                return JsonResponse({'code': 400, 'errmsg': '参数selected有误'})

        user = request.user
        if user is not None and user.is_authenticated:
            redis_conn = get_redis_connection('carts')
            cart = redis_conn.hgetall('carts_%s' % user.id)
            sku_id_list = cart.keys()
            if selected:
                # 全选
                redis_conn.sadd('selected_%s' % user.id, *sku_id_list)
            else:
                # 取消全选
                redis_conn.srem('selected_%s' % user.id, *sku_id_list)
            return JsonResponse({'code': 0, 'errmsg': '全选购物车成功'})
        else:
            # 用户已登录，操作cookie购物车
            cart = request.COOKIES.get('carts')
            if cart is not None:
                cart = pickle.loads(base64.b64decode(cart.encode()))
            else:
                cart = {}

            for sku_id in cart:
                cart[sku_id]['selected'] = selected
            cookie_cart = base64.b64encode(pickle.dumps(cart)).decode()
            response = JsonResponse({'code': 0, 'errmsg': '全选购物车成功'})
            response.set_cookie('carts', cookie_cart, max_age=7 * 24 * 3600)
            # 响应json列表数据
            return response