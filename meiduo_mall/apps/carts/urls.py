from django.urls import path
from apps.carts.views import CartsView, CartsSimpleView, CartsSelectAllView
urlpatterns = [
    # 添加购物车
    path('carts/', CartsView.as_view()),
    # 获取简单购物车
    path('carts/simple/', CartsSimpleView.as_view()),
    # 购物车全选
    path('carts/selection/', CartsSelectAllView.as_view()),
]