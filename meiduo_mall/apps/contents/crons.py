# ../ 当前目录的上一级目录，也就是 base_dir
import sys
sys.path.insert(0, '../../')

# 告诉 os 我们的django的配置文件在哪里
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "meiduo_mall.settings")

# django setup
# 相当于 当前的文件 有了django的环境
import django
django.setup()

import time
# sys.path.append('..') # 表示导入当前文件的上层目录到搜索路径中
# sys.path.append('/meiduo_mall/apps/contents') # 绝对路径
from meiduo_mall import settings
from utils.goods import get_categories
from apps.contents.models import ContentCategory



def generic_meiduo_index():
    print('--------------%s-------------' % time.ctime())
    # 商品分类数据
    categories = get_categories()

    # 广告数据
    contents = {}
    content_categories = ContentCategory.objects.all()
    for cat in content_categories:
        contents[cat.key] = cat.content_set.filter(status=True).order_by('sequence')
    # 我们的首页 后边会讲解页面静态化
    # 我们把数据 传递 给 模板
    context = {
        'categories': categories,
        'contents': contents,
    }

    # 加载渲染的模板
    from django.template import loader
    index_template = loader.get_template('index.html')

    # 把数据给模板
    index_html_data = index_template.render(context)

    # 吧渲染的html 写入知道文件

    file_path = os.path.join(os.path.dirname(settings.BASE_DIR), 'front_end_pc/index.html')

    with open(file_path, 'w', encoding='utf-8') as f:
        f.write(index_html_data)

    print(file_path)

if __name__ == '__main__':
    generic_meiduo_index()