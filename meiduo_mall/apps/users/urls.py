from django.urls import path
from apps.users.views import UsernameCountView, RegisterView, MobileCountView, LoginView, LogoutView, CenterView, EmailView, EmailVerifyView
from apps.users.views import AddressCreateView, AddressView, UpdateTitleAddressView, DefaultAddressView, UpdateDestroyAddressView, ChangePasswordView
from apps.users.views import UserBrowseHistory

urlpatterns = [
    # 判断用户名是否重复
    path('usernames/<username:username>/count/', UsernameCountView.as_view()),
    # 判断手机号
    path('mobiles/<mobile:mobile>/count/', MobileCountView.as_view()),
    # 保存注册
    path('register/', RegisterView.as_view()),
    # 登陆
    path('login/', LoginView.as_view()),
    # 退出
    path('logout/', LogoutView.as_view()),
    # 登陆返回数据
    path('info/', CenterView.as_view()),
    # 保存发送邮件
    path('emails/', EmailView.as_view()),
    # 邮箱验证
    path('emails/verification/', EmailVerifyView.as_view()),
    # 添加地址
    path('addresses/create/', AddressCreateView.as_view()),
    # 查询地址
    path('addresses/', AddressView.as_view()),
    # 保存地址标题
    path('addresses/<address_id>/title/', UpdateTitleAddressView.as_view()),

    # 保存地址默认
    path('addresses/<address_id>/default/', DefaultAddressView.as_view()),
    # 删除地址
    path('addresses/<address_id>/', UpdateDestroyAddressView.as_view()),
    # 修改密码
    path('password/', ChangePasswordView.as_view()),
    # 获取历史浏览记录
    path('browse_histories/', UserBrowseHistory.as_view()),
]