from django.urls import path
from apps.verifications.views import ImageCodeView, SmsCodeView


urlpatterns = [
    # 验证码
    path('image_codes/<uuid>/', ImageCodeView.as_view()),
    # 短信
    path('sms_codes/<mobile>/', SmsCodeView.as_view()),
]