from django.shortcuts import render

# Create your views here.
from django_redis import get_redis_connection
from django.views import View
from django.http import JsonResponse, HttpResponse
from libs.captcha.captcha import captcha
from django.shortcuts import render
from libs.yuntongxun.sms import CCP
class ImageCodeView(View):
    def get(self, request, uuid):
        # 生成图片验证码
        text, image = captcha.generate_captcha()

        # 保存图片验证码
        redis_cli = get_redis_connection('code')
        redis_cli.setex(uuid, 300, text)

        # 响应图片验证码
        return HttpResponse(image, content_type='image/jpeg')

class SmsCodeView(View):
    def get(self, request, mobile):
        # 1. 获取请求数据
        image_code = request.GET.get('image_code')
        uuid = request.GET.get('image_code_id')

        # 2.验证参数
        if not all([image_code, uuid]):
            return JsonResponse({'code': 400, 'errmsg': '参数不全'})

        # 3.验证图片验证码
        # 3.1 连接redis
        redis_cli = get_redis_connection('code')
        redis_image_code = redis_cli.get(uuid)
        if redis_image_code is None:
            return JsonResponse({'code': 400, 'errmsg': '图片验证码已过期'})
        if redis_image_code.decode().lower() != image_code.lower():  # 转小写后比较
            return JsonResponse({'code': 400, 'errmsg': '图片验证码不对'})
        send_flag = redis_cli.get('send_flag_%s' % mobile)
        if send_flag is not None:
            return JsonResponse({'code': 400, 'errmsg': '发送短信过于频繁'})

        # 4.生成短信验证码
        from random import randint
        sms_code = '%06d'%randint(0, 999999)
        pipeline = redis_cli.pipeline()
        # 5.保存短信验证码
        pipeline.setex(mobile, 300, sms_code)
        # 5.1保存手机号重复发送标识
        pipeline.setex('send_flag_%s' % mobile, 60, 1)
        pipeline.execute()
        # 6.发送短信验证码
        # CCP().send_template_sms(mobile, [sms_code, 5], 1)
        # 使用异步redis
        from celery_tasks.sms.tasks import celery_send_sms_code
        # delay 的参数 等于 任务的参数
        # Celery异步发送短信验证码
        celery_send_sms_code.delay(mobile, sms_code)
        # 7.返回响应
        return JsonResponse({'code': 0, 'errmsg': '短信验证码发送成功'})

