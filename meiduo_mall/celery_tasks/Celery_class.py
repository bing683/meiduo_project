
# 任务队列-中间人
class Broker (object):
    broker_list = []

# 消费者
class Worker(object):
    def run(self, broker, func):
        if func in broker.broker_list:
            func()
        else:
            return 'error'

# 定义celery 把 生产者、消费者、中间人 联系起来
class Celery(object):
    def __init__(self):
        self.broker = Broker()
        self.worker = Worker()

    def add(self, func):
        self.broker.broker_list.append(func)

    def work(self, func):
        self.worker.run(self.broker, func)

# 生产者
def send_sms_code():
    print('send_sms_code')

app = Celery()
app.add(send_sms_code)
app.work(send_sms_code)
